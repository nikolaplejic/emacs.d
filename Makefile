.PHONY: install uninstall reinstall

install:
	mkdir -p ~/.emacs.d/config
	ln -sf `pwd`/init.el ~/.emacs.d/init.el
	ln -sf `pwd`/config/general.el ~/.emacs.d/config/general.el
	ln -sf `pwd`/config/orgc.el ~/.emacs.d/config/orgc.el
	ln -sf `pwd`/config/evilc.el ~/.emacs.d/config/evilc.el
	ln -sf `pwd`/config/programming.el ~/.emacs.d/config/programming.el

uninstall:
	rm -rf ~/.emacs.d

reinstall: uninstall install
