;; -*- lexical-binding: t; -*-

;; ----------------------------------------------------------------------------
;; use-package
;; ----------------------------------------------------------------------------

(require 'package)
(add-to-list 'package-archives '("gnu"   . "https://elpa.gnu.org/packages/"))
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/"))
(package-initialize)

(setq use-package-always-ensure t)

;; ----------------------------------------------------------------------------
;; config
;; ----------------------------------------------------------------------------

(push "~/.emacs.d/config" load-path)

(load "general")
(load "evilc")
(load "orgc")
(load "programming")
