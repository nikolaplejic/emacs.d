;; -*- lexical-binding: t; -*-

;; compile

(use-package popwin
  :config (progn
            (popwin-mode 1)
            (push '("*compilation*" :height 30) popwin:special-display-config)))

(setq compilation-scroll-output t)

;; company

(use-package company
  :init (add-hook 'after-init-hook 'global-company-mode))

;; eglot / LSP support

(use-package eglot
  :custom (tab-width 4)
  :hook ((python-mode . eglot-ensure)
         (python-ts-mode . eglot-ensure)
         (typescript-ts-mode . eglot-ensure)
         (clojure-mode . eglot-ensure)
         (rust-mode . eglot-ensure))
  :config (progn
            (add-to-list 'eglot-server-programs
                         '((js-mode typescript-mode (typescript-ts-base-mode :language-id "typescript")) .
                           (eglot-deno "deno" "lsp")))

            (defclass eglot-deno (eglot-lsp-server) ()
              :documentation "A custom class for deno lsp.")

            (cl-defmethod eglot-initialization-options ((server eglot-deno))
              "Passes through required deno initialization options"
              (list :deno.enable t
                    :deno.lint t
                    :deno.suggest.autoImports t
                    :deno.suggest.imports.autoDiscover t))))

(with-eval-after-load 'eglot
  (add-to-list 'eglot-server-programs
               '(swift-mode . ("xcrun" "sourcekit-lsp")))
  (add-to-list 'eglot-server-programs
               '(clojure-mode . ("clojure-lsp"))))

;; rust

(use-package rust-mode)
(use-package toml-mode)

;; clojure

(use-package clojure-mode)
(use-package paredit
  :hook ((clojure-mode . enable-paredit-mode)))
(use-package monroe)

;; scheme

(use-package geiser-chez)

;; python

(use-package pyvenv)

(use-package python-ts-mode
  :ensure f
  :mode (("\\.py\\'" . python-ts-mode)))

;; typescript

(use-package typescript-mode)
(use-package typescript-ts-mode
  :mode ("\\.ts\\'" . typescript-ts-mode))

;; sicp

(use-package sicp)

;; swift

(use-package swift-mode)

;; breadcrumbs

(use-package breadcrumb
  :hook ((python-ts-mode . breadcrumb-mode)
         (markdown-mode . breadcrumb-mode)))

;; project root override

(defun np/project-override (dir)
  (let ((override (locate-dominating-file dir "project.clj"))
        (backend (ignore-errors (vc-responsible-backend dir))))
    (if override
      (list 'vc backend override)
      nil)))

(add-hook 'project-find-functions #'np/project-override)

;; line numbers

(use-package display-line-numbers
  :hook ((prog-mode-hook . display-line-numbers-mode)))
