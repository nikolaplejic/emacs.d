(setq evil-want-keybinding nil)

(use-package evil-leader
  :ensure t
  :init (global-evil-leader-mode)
  :config
  (progn
   (evil-leader/set-leader "<SPC>")
   (evil-leader/set-key
    ":"  'execute-extended-command
    "ff" 'find-file
    "bb" 'switch-to-buffer
    "bd" 'kill-current-buffer
    "qq" 'evil-quit-all
    "w"  evil-window-map
    "gg" 'magit-status
    ;; project
    "pf" 'project-find-file
    ;; org & org-roam
    "rb" 'org-roam-buffer-toggle
    "rr" 'org-roam-node-find
    "ri" 'org-roam-node-insert
    "op" 'org-mark-ring-goto))
  (evil-set-initial-state 'deadgrep-mode 'emacs))

(use-package evil
  :ensure t
  :init (evil-mode))

(use-package evil-escape
  :ensure t
  :init (evil-escape-mode)
  :custom
  (evil-escape-key-sequence "fd"))

(use-package evil-collection
  :ensure t
  :init (evil-collection-init '(doc-view dired ediff)))
