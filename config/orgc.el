(use-package org
  :hook
  (org-mode . visual-line-mode)
  :init
  (setq org-directory "~/Sync/Org/")
  (setq org-agenda-files
        '("~/Sync/Org/life.org"
          "~/Sync/Org/neutrino.org"))
  (setq org-agenda-span 'day)
  (setq org-agenda-start-day "+0d")
  (setq org-indirect-buffer-display 'current-window
    org-eldoc-breadcrumb-separator " → "
    org-enforce-todo-dependencies t
    org-entities-user
    '(("flat"  "\\flat" nil "" "" "266D" "♭")
        ("sharp" "\\sharp" nil "" "" "266F" "♯"))
    org-fontify-done-headline t
    org-fontify-quote-and-verse-blocks t
    org-fontify-whole-heading-line t
    org-hide-leading-stars t
    org-image-actual-width nil
    org-imenu-depth 8
    ;; Sub-lists should have different bullets
    org-list-demote-modify-bullet '(("+" . "-") ("-" . "+") ("*" . "+") ("1." . "a."))
    org-priority-faces
    '((?A . error)
      (?B . warning)
      (?C . success))
    org-startup-indented t
    org-tags-column 0
    org-use-sub-superscripts '{})

  (setq org-confirm-babel-evaluate nil)
  (org-babel-do-load-languages
   'org-babel-load-languages '((scheme . t)
                               (shell . t)
                               (python . t)))

  ;; follow links in same window
  (setf (cdr (assoc 'file org-link-frame-setup)) 'find-file)

  ;; org-cite
  (setq org-cite-global-bibliography '("~/Sync/library.bib"))

  (defhydra np/hydra-org (global-map "C-M-j")
    "Org"
    ("K" outline-up-heading "heading level up")
    ("k" org-backward-heading-same-level "prev heading")
    ("j" org-forward-heading-same-level "next heading")))

(use-package org-cliplink
  :ensure t)

(use-package evil-org
  :hook
  (org-mode . evil-org-mode)
  :config
  (add-hook 'evil-org-mode-hook #'evil-normalize-keymaps)
  (evil-org-set-key-theme))

(use-package org-cliplink)

;; ----------------------------------------------------------------------------
;; custom functions
;; ----------------------------------------------------------------------------

(defun np/load-agenda ()
  (interactive)
  (find-file "~/Sync/Org/life.org")
  (split-window-right)
  (other-window 1)
  (org-agenda-list nil "+0d" 'day))
